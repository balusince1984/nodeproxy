var http = require('http'),
    httpProxy = require('http-proxy');

var proxy = httpProxy.createProxyServer();

var server = http.createServer(function (req, res) {
    proxy.web(req, res, {target:'http://10.0.0.12:9200'});
    //proxy.web(req, res, {target:'http://localhost:8081'});
});

server.listen(8080);

proxy.on('error', function (e) {
    console.log(e)
});